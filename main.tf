# Margarita Šabaševa, Task No.D4ML-69, Project: DevOps 4 ML Platform Application

# Configuring the AWS Provider using Static Credentials
provider "aws" {
  region     = "eu-central-1"
  access_key = "AKIA37KDUNERDXWDAYVJ"
  secret_key = "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"
}

# Creating EC2 Instance
resource "aws_instance" "instance" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.subnet.id
  user_data              = file("install_ansible_and_git.sh")
  vpc_security_group_ids = [aws_security_group.DI-69-sg.id]
  key_name               = "DI-69-pem"
  iam_instance_profile   = aws_iam_instance_profile.profile.id

  tags = {
    Name = "DI-69-ec2"
  }
}

# Specifying AMI
data "aws_ami" "ubuntu" {
  most_recent = true
  owners = ["099720109477"] // Canonical

  filter {
      name = "name"
      values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
}

# Using pre-existing VPC
variable "vpc_id" {}

data "aws_vpc" "main_vpc" {
  id = var.vpc_id // vpc-0f5afb84b09dcbc12
}

# Public Subnet
resource "aws_subnet" "subnet" {
  vpc_id                  = data.aws_vpc.main_vpc.id
  cidr_block              = cidrsubnet(data.aws_vpc.main_vpc.cidr_block, 4, 3)
  map_public_ip_on_launch = true

  tags = {
    Name = "DI-69-subnet"
  }
}

# Route Table Associations
resource "aws_route_table_association" "table_association" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = "rtb-0b260ee7471a786e7" // Default VPC's Route Table ID
}

# Security Group Creation
resource "aws_security_group" "DI-69-sg" {
  name   = "DI-69-sg"
  vpc_id = data.aws_vpc.main_vpc.id
}

# Creating Inbound Rule (for my IP)
resource "aws_security_group_rule" "allow-https" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.DI-69-sg.id
  to_port           = 443
  type              = "ingress"
  cidr_blocks       = ["78.84.187.238/32"]
}

# Creating Outbound Rule
resource "aws_security_group_rule" "allow-outbound" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.DI-69-sg.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}

# Providing Key Pair
resource "aws_key_pair" "key_pair" {
  key_name   = "DI-69-pem"
  public_key = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCzRpGRL/RKlYhFgwUgBbtIADYMdPOn9o4m5+1NAoIPXK0Y5bXxspemSlUHa3DqaL8s0Ef6UJ0emlSe22yyeZ6JAn78glF+VUyzFc/UuMPAVDZxUMTr0ZTPoBxQaNVUX7FKBSBi/fMH+7600tpk0f3gRQkHk1canhIz89TdKiPkkX64Cm3sGrBjRwfK+TV1fJH+OIRahlqWP1u8VHQ7T82nfHa60Nv0wMY5KW74nVYxdNFRxSy4mNxE0d3QHi3SGj7AStUG83XEliB3XLVC708aU6C7jrF22tBqKW2h8966MtKMoMi9LQR/IT3molEeAXm2LtaLzKlAc/6VpVdQmwct"
}

# Creating S3 Bucket 
resource "aws_s3_bucket" "bucket" {
  bucket        = "di-69-bucket"
  acl           = "bucket-owner-full-control"
  force_destroy = true

  versioning {
    enabled = true
  }
  
  tags = {
    Name = "di-69-bucket"
    Enviroment = "Dev"
  }
}

# Creating Instance Profile
resource "aws_iam_instance_profile" "profile" {
  name = "development_profile-di-69"
  role = "${aws_iam_role.role.name}"
}

# Creating a Role for the Instance
resource "aws_iam_role" "role" {
  name = "development_role-di-69"

  assume_role_policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
          "Action": "sts:AssumeRole",
          "Principal": {
              "Service": "ec2.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
      }
    ]
  }
  EOF
}

# Creating a Policy for the Role
resource "aws_iam_role_policy" "policy" {
  name = "development_policy-di-69"
  role = aws_iam_role.role.id

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
    {
      "Effect": "Allow",
      "Action": ["s3:ListBucket"],
      "Resource": ["arn:aws:s3:::di-69-bucket"]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": ["arn:aws:s3:::di-69-bucket/*"]
    }
  ]
  }
  EOF
}
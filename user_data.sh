#! /bin/bash

# Set default SSH port to 443
perl -pi -e 's/^#?Port 22$/Port 443/' /etc/ssh/sshd_config
service sshd restart || service ssh restart

# Install Ansible
apt update -y
apt install software-properties-common -y
apt-add-repository -y --update ppa:ansible/ansible
apt install ansible -y

# Install Git
apt install git -y

# Install AWS CLI
apt-get install awscli -y

# Clone an Ansible playbook
git clone -b master --single-branch https://gitlab.com/m.sabaseva/di-69.git /tmp/ansible
#git clone https://gitlab.com/m.sabaseva/di-69.git /tmp/ansible

# Execute playbook
cd /tmp/ansible
ansible-playbook --connection=local --inventory 127.0.0.1, nginx.yml

# Create output file
nginx -v 2>>output.txt

# Copy output file from EC2 to S3
aws s3 cp output.txt s3://di-69-bucket
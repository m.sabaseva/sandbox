variable "aws_region" {
  default = "eu-central-1"
}

variable "module_name" {
  default = "AIOps"
}

variable "access_key" {
  default = "AKIA37KDUNERDXWDAYVJ"
}

variable "secret_key" {
  default = "uPkL2i/Q2S57baBtEtJlQ92dSuNnvj3udJy5+Gc3"
}

variable "ami_owners" {
  default = "099720109477" // Canonical
}

variable "ami_filter" {
  default = "ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"
}

variable "vpc_id" {
    default = "vpc-0f5afb84b09dcbc12"
}

variable "subnet_name" {
  default = "DI-69-subnet"
}

variable "subnet_cidr_block" {
    default = "172.31.48.0/20"
}

variable "route_table_id" {
    default = "rtb-0b260ee7471a786e7" // Default VPC's Route Table ID
}

variable "sg_name" {
  default = "DI-69-sg"
}

variable "my_ip" {
  default = "78.84.187.238/32"
}

variable "public_key" {
    default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCzRpGRL/RKlYhFgwUgBbtIADYMdPOn9o4m5+1NAoIPXK0Y5bXxspemSlUHa3DqaL8s0Ef6UJ0emlSe22yyeZ6JAn78glF+VUyzFc/UuMPAVDZxUMTr0ZTPoBxQaNVUX7FKBSBi/fMH+7600tpk0f3gRQkHk1canhIz89TdKiPkkX64Cm3sGrBjRwfK+TV1fJH+OIRahlqWP1u8VHQ7T82nfHa60Nv0wMY5KW74nVYxdNFRxSy4mNxE0d3QHi3SGj7AStUG83XEliB3XLVC708aU6C7jrF22tBqKW2h8966MtKMoMi9LQR/IT3molEeAXm2LtaLzKlAc/6VpVdQmwct"
}

variable "key_name" {
  default = "DI-69-pem"
}

variable "instance_name" {
  default = "DI-69-ec2"
}

variable "instance_type" {
  default = "t2.micro"
}


variable "bucket" {
  default = "di-69-bucket"
}

variable "profile_name" {
  default = "development_profile-di-69"
}

variable "role_name" {
  default = "development_role-di-69"
}

variable "policy_name" {
  default = "development_policy-di-69"
}

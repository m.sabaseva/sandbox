# Margarita Šabaševa, Task No.D4ML-69, Project: DevOps 4 ML Platform Application

# Initializing Provider using Static Credentials
provider "aws" {
  region     = var.aws_region
  access_key = var.access_key
  secret_key = var.secret_key
}

# Specifying AMI
data "aws_ami" "ubuntu" {
  most_recent = true
  owners      = [var.ami_owners]

  filter {
      name   = "name"
      values = [var.ami_filter]
  }
}

# Public Subnet
resource "aws_subnet" "subnet" {
  vpc_id                  = var.vpc_id
  cidr_block              = var.subnet_cidr_block
  map_public_ip_on_launch = true

  tags = {
    Name = "${lower(var.module_name)}.${var.subnet_name}"
  }
}

# Route Table Associations
resource "aws_route_table_association" "table_association" {
  subnet_id      = aws_subnet.subnet.id
  route_table_id = var.route_table_id
}

# Security Group Creation
resource "aws_security_group" "DI-69-sg" {
  name   = var.sg_name
  vpc_id = var.vpc_id
}

# Creating Inbound Rule (for my IP)
resource "aws_security_group_rule" "allow-https" {
  from_port         = 443
  protocol          = "tcp"
  security_group_id = aws_security_group.DI-69-sg.id
  to_port           = 443
  type              = "ingress"
  cidr_blocks       = [var.my_ip]
}

# Creating Outbound Rule
resource "aws_security_group_rule" "allow-outbound" {
  from_port         = 0
  protocol          = "-1"
  security_group_id = aws_security_group.DI-69-sg.id
  to_port           = 0
  type              = "egress"
  cidr_blocks       = ["0.0.0.0/0"]
}

# Providing Key Pair
resource "aws_key_pair" "key_pair" {
  key_name   = var.key_name
  public_key = var.public_key
  }

# Creating EC2 Instance
resource "aws_instance" "instance" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = var.instance_type
  subnet_id              = aws_subnet.subnet.id
  user_data              = file("user_data.sh")
  vpc_security_group_ids = [aws_security_group.DI-69-sg.id]
  key_name               = var.key_name
  iam_instance_profile   = aws_iam_instance_profile.profile.id

  tags = {
    Name = "${lower(var.module_name)}.${var.instance_name}"
  }
}

# Creating S3 Bucket 
resource "aws_s3_bucket" "bucket" {
  bucket        = var.bucket
  force_destroy = true

  versioning {
    enabled = true
  }
  
  tags = {
    Name = "${lower(var.module_name)}.${var.bucket}"
  }
}

# Creating Instance Profile
resource "aws_iam_instance_profile" "profile" {
  name = var.profile_name
  role = aws_iam_role.role.name
}

# Creating a Role for the Instance
resource "aws_iam_role" "role" {
  name = var.role_name

  assume_role_policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
      {
          "Action": "sts:AssumeRole",
          "Principal": {
              "Service": "ec2.amazonaws.com"
          },
          "Effect": "Allow",
          "Sid": ""
      }
    ]
  }
  EOF
}

# Creating a Policy for the Role
resource "aws_iam_role_policy" "policy" {
  name = var.policy_name
  role = aws_iam_role.role.id

  policy = <<-EOF
  {
    "Version": "2012-10-17",
    "Statement": [
    {
      "Effect": "Allow",
      "Action": ["s3:ListBucket"],
      "Resource": ["arn:aws:s3:::di-69-bucket"]
    },
    {
      "Effect": "Allow",
      "Action": [
        "s3:PutObject",
        "s3:GetObject",
        "s3:DeleteObject"
      ],
      "Resource": ["arn:aws:s3:::di-69-bucket/*"]
    }
  ]
  }
  EOF
}